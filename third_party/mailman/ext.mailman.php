<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Mailman
*
* @package Mailman
* @author  Joel Zerner
* @link    https://bitbucket.org/joelzerner/mailman/
*/

class Mailman_ext {

	public $name            = 'Mailman';
	public $version         = '1.2';
	public $description     = 'Site-wide customisable HTML email templates.';
	public $docs_url        = 'https://bitbucket.org/joelzerner/mailman/';
	public $settings_exist  = 'y';
	public $settings        = array();

	public function __construct($settings = '')
	{
		$this->EE =& get_instance();
		$this->EE->load->add_package_path(PATH_THIRD .'mailman/');
		$this->settings = $settings;
	}

	public function settings()
	{
    $settings = array();

    $settings['automatic']      					= array('r', array(1 => "Yes", 0 => "No"), 'y');
    $settings['automatic_template_name']  = array('i', '', 'default');
    $settings['mail_type']    						= array('s', array('html' => 'HTML', 'plain' => 'Plain Text'), 'html');

    return $settings;
	}

	public function activate_extension()
	{

		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');

		$this->settings = array(
			'automatic'								=> 1,
			'automatic_template_name' => 'default',
			'mail_type'     					=> 'html'
		);

		$data = array(
			'class' => __CLASS__,
			'settings' => '',
			'priority' => 10,
			'version' => $this->version,
			'enabled' => 'y',
			'hook' => 'email_send',
			'method' => 'email_send');

		$this->EE->db->insert('exp_extensions', $data);

	}

	public function disable_extension()
	{
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');
	}

	public function update_extension($current = '')
	{
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}

		if ($current < '1.0')
		{
			// Update to version 1.0
		}

		ee()->db->where('class', __CLASS__);
		ee()->db->update(
			'extensions',
			array('version' => $this->version)
		);
	}

	// function generate random string
	public function gen_rand_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $characters_length = strlen($characters);
    $random_string = '';
    for ($i = 0; $i < $length; $i++) {
        $random_string .= $characters[rand(0, $characters_length - 1)];
    }
    return $random_string;
	}

	// function find the message id for boundaries
  public function find_message_id($content = '', $content_alt = '') {
    preg_match('/boundary\=\"([a-zA-Z0-9_]+)\"/', $content, $matches);
    if (count($matches)) {
      return $matches[1];
    } else {
    	preg_match('/boundary\=\"([a-zA-Z0-9_]+)\"/', $content_alt, $matches);
      if (count($matches)) {
        return $matches[1];
      } else {
      	preg_match('/Message\-ID: \<([a-zA-Z0-9]+)@/', $content, $matches);
      	if (count($matches)) {
	        return $matches[1];
	      } else {
	        return 'B_' . $this->gen_rand_string();
	      }
        return 'B_' . $this->gen_rand_string();
      }
    }
  }

  // function strip content type plain or html
  public function strip_content_string($content) {
    $pattern = "/Content\-Type\: [a-zA-Z0-9\/]+\;.*?charset\=[a-zA-Z0-9-]+.*?Content\-Transfer\-Encoding\: [a-zA-Z0-9-]+/s";
    return preg_replace($pattern, '', $content);
  }

	public function email_send($data)
	{


		// set vars
		$automatic = $this->settings['automatic']; // if true, does not require {mailtemplate_x} tag to be used
		$automatic_template_name = $this->settings['automatic_template_name']; // template to use if automatic is set to true and not tag is found in the mail body content
		$mail_type = $this->settings['mail_type']; // "html" or "plain"
		$template_name = ""; // leave blank
		$template = 0;
		$debug = 0;
		$mail_protocol = ee()->config->config['mail_protocol'];
		$site_label = ee()->config->config['site_label'];
		$site_url = ee()->config->config['site_url'];
		$sent_from = '<strong>' . $site_label . '</strong><br /><a href="' . $site_url . '">' . $site_url . '</a>';

    // debug
    if ($debug) {
			echo '<pre>'; print_r($data['header_str']); echo '</pre>';
			echo '<pre>'; print_r('---BODY---'); echo '</pre>';
			echo '<pre>'; print_r($data['finalbody']); echo '</pre>';
    }

    // get ee data
    $body = $data['finalbody'];
    $mail_header = $data['header_str'];

		// check if and how mailman will be used, and what template to use
    preg_match('/\{mailtemplate_([a-zA-Z0-9_-]+)\}/', $body, $template_matches);
    if (count($template_matches)) {
      $template_name = $template_matches[1];
    } else if ($automatic && $automatic_template_name) {
      $template_name = $automatic_template_name;
    }

    // get the template if it exists
    if (strlen($template_name)) {
      $filename = PATH_THIRD . 'mailman/templates/' . $template_name . '.html';
      if (file_exists($filename)) {
        $template = file_get_contents($filename);
      }
    }

    // if there's a template name and it exists then we're running it!
    if ($template_name && $template) {

      // get message_id
      $message_id = $this->find_message_id($mail_header, $body);
      $alt_id = 'B_ALT_' . $this->gen_rand_string();

      // set boundary
      $boundary = "--" . $message_id;
      $boundary_pattern = '/\-\-' . $message_id . '/';
      $boundary_alt = "--" . $alt_id;
      $boundary_alt_pattern = '/\-\-' . $alt_id . '/';

      // set mime section vars
      $section_multi = 'Content-Type: multipart/alternative; boundary="' . $alt_id . '"';
      $section_html = 'Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit';
      $section_plain = 'Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit';
			$part_plain = '';
			$part_html = '';

      // if it's a multi part message, get all the parts
      $body_after = '';
      if (stristr($body, 'This is a multi-part message in MIME format.')) {

        $split = preg_split($boundary_pattern, $body);

        foreach ($split as $match) {

        	if (stristr($match, 'multipart/alternative')) {

        		$alt_id = $this->find_message_id($match);
      			$boundary_alt = "--" . $alt_id;
      			$boundary_alt_pattern = '/\-\-' . $alt_id . '/';
      			$section_multi = 'Content-Type: multipart/alternative; boundary="' . $alt_id . '"';
        		$alt_split = preg_split($boundary_alt_pattern, $body);

        		foreach ($alt_split as $alt) {
        			if (stristr($alt, 'text/plain')) {
		            $part_plain = $this->strip_content_string($alt);
		            $body = $part_plain;
		          } else if (stristr($alt, 'text/html')) {
		            $part_html = $this->strip_content_string($alt);
		            $body = $part_html;
		          }
        		}

          } else if (stristr($match, 'text/plain')) {
		        $part_plain = $this->strip_content_string($match);
            $body = $part_plain;
          } else if (stristr($match, 'text/html')) {
		        $part_html = $this->strip_content_string($match);
            $body = $part_html;
          } else if (stristr($match, 'Content-Disposition: attachment;')) {
            $body_after .= $boundary . $match;
          }
        }

      } else {
      	$body = $this->strip_content_string($body);
      }

      // remove the template string from the body & trim whitespace
      $body = preg_replace("/\{mailtemplate_([a-zA-Z0-9_-]+)\}/", "", $body);
      $body = trim($body);

      // format plain text body
      if ($part_plain) {
        $body_plain = $part_plain;
      } else {
				$tags = array('</p>','<br />','<br>','<hr />','<hr>','</h1>','</h2>','</h3>','</h4>','</h5>','</h6>');
				$body_plain = str_replace($tags,"\n",$body);
				$body_plain = preg_replace('/\<a href\=\"(?=https\:\/\/|http\:\/\/)(.+?)\"\>(.+?)\<\/a\>/si','$2 $1',$body_plain);
        $body_plain = strip_tags($body_plain);
      }

      // format html body
      if ($part_html) {
        $body_html = $part_html;
        $body_html = str_replace("=3D","=",$body_html);
      } else {
        $body_html = str_replace("\r", "", $body);  // Remove \r
        $body_html = str_replace("\n", "<br />", $body_html);  // Replace \n with <br />
        $body_html = "<p>" . $body_html . "</p>";
      }

      // if html and template exists, set up the html email, otherwise convert to plaintext
      if ($mail_type=='html' && $template) {
        require_once('lib/instyle.php');
        $instyle = new InStyle();
        $email_content = str_replace("{body}", $body_html, $template);
				$email_content = str_replace("{sent_from}", $sent_from, $email_content);
				$email_content = str_replace("{site_url}", $site_url, $email_content);
        $message_html = $instyle->convert($email_content);
      } else {
        $message_html = false;
      }

      // set message plain
      $message_plain = $body_plain;

      // build mail headers
      // echo '<pre>'; print_r('BA: ' . $body_after); echo '</pre>'; die();
      if ($body_after) {
      	$mime_type_header = 'Content-Type: multipart/mixed; boundary="' . $message_id . '"';
      } else {
      	$mime_type_header = 'Content-Type: multipart/alternative; boundary="' . $message_id . '"';
      }
      $pattern = "/Content\-Type\: [a-zA-Z0-9\/]+\;.*?charset\=[a-zA-Z0-9-]+.*?Content\-Transfer\-Encoding\: [a-zA-Z0-9-]+/s";
      $mail_header = preg_replace($pattern, $mime_type_header, $mail_header);
      if (!stristr($mail_header, $mime_type_header)) {
      	$mail_header .= $mime_type_header;
      }

      // build message
      $message = 'This is a multi-part message in MIME format.
Your email application may not support this format.';
			if (stristr($mail_header, 'multipart/mixed') && $message_plain) {
				if ($message_plain && $message_html) $message .= PHP_EOL . PHP_EOL . $boundary . PHP_EOL . $section_multi;
				if ($message_plain) $message .= PHP_EOL . PHP_EOL . $boundary_alt . PHP_EOL . $section_plain . $message_plain;
      	if ($message_html) $message .= PHP_EOL . $boundary_alt . PHP_EOL . $section_html . $message_html;
      	if ($message_plain && $message_html) $message .= PHP_EOL . $boundary_alt . '--';
			} else {
				if ($message_plain) $message .= PHP_EOL . PHP_EOL . $boundary . PHP_EOL . $section_plain . PHP_EOL . PHP_EOL . $message_plain;
      	if ($message_html) $message .= PHP_EOL . $boundary . PHP_EOL . $section_html . PHP_EOL . PHP_EOL . $message_html;
			}
      if ($body_after) $message .= PHP_EOL . PHP_EOL . $body_after;
      $message .= PHP_EOL . $boundary . '--';

      // return ee data
      $data['finalbody'] = $message;
      $data['header_str'] = $mail_header . PHP_EOL;

    }

    // debug
    if ($debug) {
      echo '<pre>'; print_r($data['header_str']); echo '</pre>';
      echo '<br><br>';
      echo '<pre>'; print_r($data['finalbody']); echo '</pre>';
      die();
    }

    return true;

	}

}




?>