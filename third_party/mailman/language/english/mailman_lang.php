<?php

// load dependencies
require_once PATH_THIRD.'assets/config.php';

$lang = array(

'automatic' => 'Run automatically on all email messages?',
'automatic_template_name' => 'Automatic template name',
'mail_type' => 'Mail Type'

);
