Mailman v1.0
================================

Site-wide customisable HTML email templates on Expression Engine v2.

Setup
--------------------------------

1. Move the /mailman/ directory into the /expressionengine/third_party/ directory.
2. In Expression Engine, go to Add-Ons > Extensions and enable Mailman.
3. Good to go!


Instructions
--------------------------------

1. To use Mailman in a message template in Expression Engine just include {mailtemplate_default} where "default" can be replaced with whatever the name of your template file is.
2. The templates are stored in /mailman/templates/ and must be HTML files.
3. Each template must include the {body} tag in it where you would like the email content to be inserted.